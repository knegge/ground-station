# Ground-station

This is a simple python based ground station that operates an SDR and a rotator to capture a single satellite when 
it is above the horizon.<br>
It is running under docker but should be possible to run on the host directly if the software dependencies are met.

## Installation
On the host you will be needing docker engine and docker compose (min v1.27), additional udev rules and module blacklists might 
be required depending on the hardware you will be using. 
Refer to this [guide](https://github.com/kng/satnogs-client-docker/blob/main/lsf/LSF-GUIDE.md#udev-rules-and-module-blacklist).<br>
If you are using Debian 12 or Ubuntu 22 the installation should be pretty straightforward as the packages are new enough.
```
sudo apt install docker.io apparmor docker-compose
sudo adduser $(whoami) docker
```

## Usage

From here on, do note that hosts differ in the compose command, `docker-compose` or `docker compose`.<br>

First copy the default [env](station.env-dist) and edit it, `cp station.env-dist station.env` and use your favourite editor.
From here on this file will be referred to as __env__.<br>
Use `docker-compose build` to build the image.<br>

By default, the directory `srv` is bind-mounted into the container to `/srv` making it easy to collect and store 
files. Make sure it has the appropriate permissions, `chmod 0777 srv` for the lazy.<br>

When testing this it can be easier to run the commands manually, like:
`docker-compose run --rm groundstation worker.py -f 435.605e6 -n 44909`<br>
When everything is running ok, set the `COMMAND` in the env and bring it up with: 
`docker-compose up`


## Operation

The runtime settings are located in the station.env file. 

The main program is the [worker](python/worker.py).
It waits for the satellite to rise above the horizon and then starts tracking it and record 
an [iq-wav](https://wiki.gnuradio.org/index.php?title=Wav_File_Sink), 
a waterfall, demodulated data and a log from the rotator.

Procedure:
* Load TLE from network or file
* Calculate the rise time
* Wait for rise time
* Generate [doppler correction](https://destevez.net/2022/07/real-time-doppler-correction-with-gnu-radio/) file
* Launch gr-satellites
* Launch the [flowgraph](grc/flowgraph.sh)
* Track the satellite
* Stop the flowgraph and gr-satellites when satellite is below horizon
* Generate waterfall PNG, rename and compress the results

## GPIO

Supporting the [mcp2221](https://www.adafruit.com/product/4471).<br> 
For controlling relays to enable LNA, choosing antennas etc, it is possible to use the [gpio.py](python/gpio.py) 
to run in [PREPOST_SCRIPT](python/prepost.py). See [satnogs.py](python/satnogs.py) for example.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Main packages included
[GNU radio 3.10](https://www.gnuradio.org/)<br>
[gr-satellites](https://github.com/daniestevez/gr-satellites)<br>
[satellitetle](https://gitlab.com/librespacefoundation/python-satellitetle)<br>
[PyEphem](https://rhodesmill.org/pyephem/)<br>
[Skyfield](https://rhodesmill.org/skyfield/)<br>
[SoapySDR](https://github.com/pothosware/SoapySDR)<br>

## License

[AGPL](https://choosealicense.com/licenses/agpl-3.0/)