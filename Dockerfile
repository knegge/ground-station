# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG IMAGE_TAG=jammy
FROM ubuntu:${IMAGE_TAG}

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC

WORKDIR /usr/src/
COPY packages.ubuntu .
RUN apt update &&\
    apt install -y software-properties-common &&\
    add-apt-repository ppa:daniestevez/gr-satellites &&\
    apt update &&\
    apt upgrade -qy &&\
	xargs -a packages.ubuntu apt install -qy &&\
	rm -r /var/lib/apt/lists/*

RUN uhd_images_downloader -q
COPY packages.pip .
RUN xargs -a packages.pip pip install

COPY grc/*.sh python/*.py /usr/local/bin/
COPY gnuradio/*.conf /etc/gnuradio/conf.d/
COPY satyaml/* /usr/lib/python3/dist-packages/satellites/satyaml/
COPY grc/*.grc .
RUN grcc -o /usr/local/bin *.grc &&\
    chmod 0755 /usr/local/bin/*
WORKDIR /
