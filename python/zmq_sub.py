#!/usr/bin/env python3
import os
from gnuradio.gr import pmt
import zmq

port = int(os.getenv('ZMQ_PORT', '5001'))
host = os.getenv('ZMQ_HOST', 'localhost')
print(f'ZMQ SUB Connecting to {host}:{port}')
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect(f'tcp://{host}:{port}')
socket.subscribe('')

while True:
    try:
        msg = socket.recv()
        pdu = pmt.deserialize_str(msg)
        data = pmt.to_python(pmt.cdr(pdu)).tobytes()
        print(f'ZMQ Data: {data}')
    except KeyboardInterrupt:
        break
