#!/usr/bin/env python3
import docker
from sys import argv
try:
    from gpio import set_outputs
except ImportError:
    def set_outputs(arg: dict):
        print('gpio not found')

# This needs docker.sock to be bind-mounted in the container
#     volumes:
#       - type: 'bind'
#         source: '/var/run/docker.sock'
#         target: '/var/run/docker.sock'
#
# The services needs to be named 'satnogs_client' and 'satnogs_auto_scheduler'


def main():
    if len(argv) != 2:
        print(f'Useage: {argv[0]} <pre|post>')
        exit(0)

    client = docker.DockerClient(base_url='unix://var/run/docker.sock')

    if 'pre' in argv[1]:  # stop satnogs-client
        for c in client.containers.list(filters={'name': 'satnogs_client'}, all=True):
            c.stop(timeout=1)
        for c in client.containers.list(filters={'name': 'satnogs_auto_scheduler'}, all=True):
            c.stop(timeout=1)
        set_outputs({'lna': 1, 'pa': 0, 'freq': 0})  # set GPIO to default state

    elif 'post' in argv[1]:  # start satnogs-client
        for c in client.containers.list(filters={'name': 'satnogs_client'}, all=True):
            c.start()
        for c in client.containers.list(filters={'name': 'satnogs_auto_scheduler'}, all=True):
            c.start()
    else:
        print('Unknown command')


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(f'Unhandled exception: {e}')
