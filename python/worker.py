#!/usr/bin/env python3
import argparse
import datetime
import logging
import os
import socket
import struct
import subprocess
import sys
import time

import ephem
import matplotlib.pyplot as plt
import numpy as np
import satellite_tle

logging.basicConfig(
    format="%(name)s - %(levelname)s - %(message)s",
    level=getattr(
        logging, os.getenv("LOG_LEVEL", "INFO")
    ),
)
LOGGER = logging.getLogger("worker")


class Worker(object):
    def __init__(self, mode=0, norad=0, samp_rate=0, freq=0):
        self.mode = mode
        self.norad = norad
        self.samp_rate = samp_rate
        self.freq = freq

        self.length = 3600  # generate doppler file this long
        self.tle_age = ephem.now()
        self.flip = False
        self.ts = None
        self.ts_str = ''
        self.rotor = socket.socket()
        self.rotor.close()
        self.udp_host = os.getenv('UDP_SINK_HOST', '0.0.0.0')
        self.udp_port = int(os.getenv('UDP_SINK_PORT', '57356'))
        self.rot_host = os.getenv('ROT_HOST', '')
        self.rot_port = int(os.getenv('ROT_PORT', '4533'))
        self.rot_threshold = float(os.getenv('ROT_THRESHOLD', '4.0'))
        self.iq_file = os.getenv('IQ_FILE_PATH', '/tmp/iq.wav')
        self.wf_file = os.getenv('WATERFALL_FILE_PATH', '/tmp/iq.wf')
        self.wf_image = os.getenv('WATERFALL_IMAGE_PATH', '/tmp/iq.png')
        self.kiss_file = os.getenv('KISS_FILE_PATH', '/tmp/iq.kiss')
        self.doppler_file = os.getenv('DOPPLER_FILE_PATH', '/tmp/doppler.txt')
        self.rotlog_file = os.getenv('ROTLOG_FILE_PATH', '/tmp/rotor.log')
        self.flowgraph = os.getenv('FLOWGRAPH', 'recorder.py')
        self.flowgraph_pid = os.getenv('FLOWGRAPH_PID', '/tmp/flowgraph.pid')
        self.grsat_pid = os.getenv('GRSAT_PID', '/tmp/grsat.pid')
        self.tle_source = os.getenv('TLE_SOURCE', '')
        self.tle_hours = float(os.getenv('TLE_HOURS', '24'))
        self.zmq_port = os.getenv('ZMQ_PORT', '5001')
        self.prepost_script = os.getenv('PREPOST_SCRIPT', '')
        self.keep_results = bool(os.getenv('KEEP_RESULTS', 'False'))
        self.extra_time = float(os.getenv('EXTRA_TIME', '0')) * ephem.second
        self.min_elevation = float(os.getenv('MIN_ELEVATION', '0'))

        self.satellite = ephem.EarthSatellite()
        self.observer = ephem.Observer()
        try:
            self.observer.lat = os.getenv('STATION_LAT')
            self.observer.lon = os.getenv('STATION_LON')
            self.observer.elevation = float(os.getenv('STATION_ELEV', '0'))
            self.rx_rate = os.getenv('RX_SAMP_RATE')
            self.rx_device = os.getenv('RX_DEVICE')
        except TypeError:
            LOGGER.error('Missing station configuration.')
            exit(3)

    def main(self):
        if self.mode == 0 or self.mode == 1:
            if self.mode == 0:
                LOGGER.info('Looping observations')
            else:
                LOGGER.info('Single observation')
            while True:
                if self.tle_age <= ephem.now():
                    self.create_satellite()
                self.next_pass()
                LOGGER.info(f'Next observation starts: {self.ts_str}')
                while self.ts > ephem.now() + self.extra_time:
                    time.sleep(1)
                self.start_prepost('pre')
                self.generate_doppler()
                self.start_grsat()
                self.start_flowgraph()
                self.start_tracking()  # this will block until done
                self.stop_process(self.flowgraph_pid, 'flowgraph')
                self.stop_process(self.grsat_pid, 'gr_satellites')
                self.manage_results()
                self.start_prepost('post')
                if self.mode != 0:
                    break
                time.sleep(10)  # if terminated, delay to prevent next observation

        elif args.mode == 3:
            LOGGER.info('Tracking only')
            self.create_satellite()
            self.next_pass()
            LOGGER.info(f'Next observation starts: {self.ts_str}')
            while self.ts > ephem.now() + self.extra_time:
                time.sleep(1)
            self.generate_doppler()
            self.start_tracking()

        elif args.mode == 9:
            LOGGER.info('Idling')
            while True:
                try:
                    time.sleep(10)
                except KeyboardInterrupt:
                    LOGGER.info('Exiting')
                    break

        else:
            LOGGER.error('Unknown mode')

    def create_satellite(self):
        sources = [('local', self.tle_source)]
        tle = list(satellite_tle.fetch_tles([self.norad], sources=sources).values())[0][1]
        try:
            self.satellite = ephem.readtle(tle[0], tle[1], tle[2])
        except ValueError:
            LOGGER.error('Could not load TLE')
            exit(1)
        self.tle_age = ephem.now() + (ephem.hour * self.tle_hours)
        LOGGER.info(f'Created satellite: {tle[0]}')

    def next_pass(self):
        self.observer.date = ephem.now()
        self.satellite.compute(self.observer)
        self.ts = None

        # check if above horizon and go back in 10min steps to find current pass
        if self.satellite.alt > 0.0:
            while self.satellite.alt > 0 and self.observer.date > ephem.now() - 1:
                self.observer.date -= ephem.minute * 10
                self.satellite.compute(self.observer)

        # search for pass within one day
        while self.observer.date < ephem.now() + 1:
            # Rise time, Rise azimuth, Maximum altitude time, Maximum altitude, Set time, Set azimuth
            p = self.observer.next_pass(self.satellite)
            if p[3].conjugate() * 180 / 3.141593 < self.min_elevation:
                LOGGER.info(f'Skip low pass on {p[0].datetime().isoformat(" ", "seconds")} '
                            f'with {p[3].conjugate() * 180 / 3.141593:.1f} max elevation')
            else:
                LOGGER.info(f'Found pass on {p[0].datetime().isoformat(" ", "seconds")} '
                            f'with {p[3].conjugate() * 180 / 3.141593:.1f} max elevation')
                self.ts = p[0]
                break
            self.observer.date = p[4]  # look for next pass
        if self.ts is None:
            LOGGER.error('No passes found, exiting.')
            exit(2)
        self.ts_str = self.ts.datetime().strftime('%Y-%m-%d_%H-%M-%S')

    def generate_doppler(self):
        LOGGER.info('Generate doppler file')

        if not self.satellite:
            LOGGER.error('Satellite not defined.')
            with open(self.doppler_file, 'w') as fp:
                fp.write('0.0 0.0')
            return False

        tstart = datetime.datetime.utcnow()
        times = [tstart + datetime.timedelta(seconds=s) for s in range(0, self.length)]

        LOGGER.info(f'Generating doppler for {self.norad}, first timestamp {tstart}')
        az_old = None
        self.flip = False
        with open(self.doppler_file, 'w') as fp:
            for t in times:
                self.observer.date = t
                self.satellite.compute(self.observer)
                freq = (1 - self.satellite.range_velocity / 299792458.0) * self.freq - self.freq
                fp.write(f'{t.timestamp()} {freq}\n')
                az = self.satellite.az.conjugate() * 180 / 3.141593
                alt = self.satellite.alt.conjugate() * 180 / 3.141593
                if alt > 0.0:  # determine if az moves over the endstop in north
                    if not az_old:
                        az_old = az
                    if abs(az - az_old) > 180.0:  # if it jumps more than half a turn in 1s
                        self.flip = True
                    az_old = az

    def start_flowgraph(self):
        LOGGER.info('Starting flowgraph')
        cmd = 'flowgraph.sh', str(self.samp_rate), str(self.freq)
        try:
            s = subprocess.Popen(cmd)
            with open(self.flowgraph_pid, 'w') as pf:
                pf.write(str(s.pid))
        except (FileNotFoundError, OSError):
            LOGGER.error(f'Unable to launch {self.flowgraph}')

    def start_tracking(self):
        LOGGER.info('Start tracking')
        LOGGER.info(f'Rotator pos: {self.hamlib_query("p")}')
        old_alt = old_az = 0.0
        t_start = self.observer.date = ephem.now()
        # Rise time, Rise azimuth, Maximum altitude time, Maximum altitude, Set time, Set azimuth
        _, rise_az, _, _, _, _ = self.observer.next_pass(self.satellite)
        rise_az = rise_az.conjugate() * 180 / 3.141593  # rize az, move rotator here before pass starts
        aos = False
        while True:
            try:
                self.observer.date = ephem.now()
                self.satellite.compute(self.observer)
                sat_alt = self.satellite.alt.conjugate() * 180 / 3.141593
                sat_az = self.satellite.az.conjugate() * 180 / 3.141593

                if ((sat_alt <= 0.0 and aos) or  # going below horizon
                        (t_start + ephem.minute * 2 < self.observer.date and not aos)):  # never rising
                    if self.extra_time > 0:
                        LOGGER.info(f'Satellite below horizon, waiting {self.extra_time / ephem.second} seconds.')
                    t_stop = ephem.now() + self.extra_time
                    while aos and t_stop > ephem.now():
                        time.sleep(1)
                    LOGGER.info('Stop tracking.')
                    cmd_az = 180.0
                    cmd_alt = 90.0
                    self.hamlib_query(f'P {cmd_az:.02f} {cmd_alt:.02f}')  # parking
                    break
                elif sat_alt <= 0.0:
                    sat_az = rise_az  # go to rise azimuth
                    sat_alt = 0.0
                elif sat_alt > 0.0:
                    aos = True

                # calculate rotator alt/az flipping
                cmd_az = (180.0 + sat_az) % 360.0 if self.flip else sat_az
                cmd_alt = 180.0 - sat_alt if self.flip else sat_alt
                cmd_alt = max(min(cmd_alt, 175.0), 5.0)  # min 5 deg alt
                try:
                    rp = self.hamlib_query('p')
                    rot_az, rot_alt = [float(i.split(':')[1]) for i in rp.split(',')]
                except (ValueError, AttributeError, IndexError):
                    rot_az = rot_alt = -1
                if abs(cmd_az - old_az) > self.rot_threshold or abs(cmd_alt - old_alt) > self.rot_threshold:
                    LOGGER.info(f'Sat: az {sat_az:.02f}, alt {sat_alt:.02f}. '
                                f'Cmd: az {cmd_az:.02f}, alt {cmd_alt:.02f}. '
                                f'Rot: az {rot_az:.02f}, alt {rot_alt:.02f}.')
                    self.hamlib_query(f'P {cmd_az:.02f} {cmd_alt:.02f}')
                    old_az = cmd_az
                    old_alt = cmd_alt
                self.rotor_log(f'{sat_az:.02f},{sat_alt:.02f},'
                               f'{old_az:.02f},{old_alt:.02f},'
                               f'{rot_az:.02f},{rot_alt:.02f}\n')
                time.sleep(1)
            except KeyboardInterrupt:
                break
        if self.rotor.fileno() > 0:
            self.rotor.close()

    def start_grsat(self):
        LOGGER.info('Starting gr-satellites')
        grsat = ['gr_satellites', str(self.norad), '--samp_rate', str(self.samp_rate), '--iq', '--udp', '--udp_raw',
                 '--udp_port', str(self.udp_port), '--ignore_unknown_args', '--use_agc', '--satcfg']
        if len(self.kiss_file) > 0:
            grsat.append('--kiss_out')
            grsat.append(self.kiss_file)
        if len(self.zmq_port) > 0:
            grsat.append('--zmq_pub')
            grsat.append(f'tcp://0.0.0.0:{self.zmq_port}')

        LOGGER.debug(' '.join(grsat))
        try:
            s = subprocess.Popen(grsat)
            with open(self.grsat_pid, 'w') as pf:
                pf.write(str(s.pid))
        except (FileNotFoundError, TypeError) as e:
            LOGGER.error(f'Unable to launch gr-satellites: {e}')

    @staticmethod
    def stop_process(pidfile, name):
        LOGGER.info(f'Stopping {name}')
        try:
            with open(pidfile, 'r') as pf:
                os.kill(int(pf.readline()), 15)
            os.unlink(pidfile)
        except (FileNotFoundError, ProcessLookupError, OSError):
            LOGGER.warning(f'No {name} running')

    @staticmethod
    def parse_kissfile(infile):
        ts = datetime.datetime.now()  # MUST be overwritten by timestamps in file
        for row in infile.read().split(b'\xC0'):
            if len(row) == 9 and row[0] == 9:  # timestamp frame
                ts = datetime.datetime(1970, 1, 1) + datetime.timedelta(seconds=struct.unpack('>Q', row[1:])[0] / 1000)
            if len(row) > 0 and row[0] == 0:  # data frame
                yield ts, row[1:].replace(b'\xdb\xdc', b'\xc0').replace(b'\xdb\xdd', b'\xdb')

    def convert_waterfall(self):
        nchan = 1024
        navg = 4

        with open(self.wf_file, mode='rb') as datafile:
            data_dtypes = np.dtype([('spec', 'float32', (nchan,))])
            data = np.fromfile(datafile, dtype=data_dtypes)
        if not data.size:
            LOGGER.warning('Empty waterfall')
            return

        data_mean = np.mean(data['spec'])
        data_std = np.std(data['spec'])
        tmax = data['spec'].shape[0] / self.samp_rate * navg * 1000
        d_freq = self.freq / 1000000.0

        plt.figure(figsize=(10, 20))
        plt.imshow(data['spec'],
                   origin='lower',
                   aspect='auto',
                   interpolation='None',
                   extent=(-self.samp_rate / 2000, self.samp_rate / 2000, 0.0, tmax),
                   vmin=data_mean - 2.0 * data_std,
                   vmax=data_mean + 6.0 * data_std,
                   cmap='viridis')
        plt.xlabel(f'Frequency (kHz), center {d_freq:3.3f} MHz')
        plt.ylabel(f'Time (seconds)')
        fig = plt.colorbar(aspect=50)
        fig.set_label('Power (dB)')
        plt.savefig(self.wf_image, bbox_inches='tight')
        plt.close()
        if not self.keep_results:
            os.remove(self.wf_file)

    def manage_results(self):
        LOGGER.info('Renaming and compressing results')
        self.wf_image = f'{os.path.dirname(self.wf_file)}/{self.norad}_{self.ts_str}.png'
        try:
            self.convert_waterfall()
        except Exception as e:
            LOGGER.error(f'Waterfall image generation failed: {e}')
        for f in [self.iq_file, self.kiss_file, self.rotlog_file, self.wf_file]:
            if not os.path.isfile(f):
                continue
            newname = f'{os.path.dirname(f)}/{self.norad}_{self.ts_str}{os.path.splitext(f)[1]}'
            try:
                os.rename(f, newname)
                subprocess.run(f"zstd --no-progress --rm -f {newname}".split())
            except (FileNotFoundError, OSError) as e:
                LOGGER.error(f'File rename and compress failed: {e}')
        try:
            os.unlink(self.doppler_file)
        except (FileNotFoundError, OSError):
            pass

    def hamlib_query(self, cmd: str):
        if len(self.rot_host) < 1 or not 0 < self.rot_port < 65536:
            return None
        try:
            if self.rotor.fileno() < 0:
                self.rotor = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.rotor.connect((self.rot_host, self.rot_port))
                self.rotor.settimeout(2.0)
            self.rotor.sendall(bytes(f'+{cmd}\n', encoding='ascii'))
            data = self.rotor.recv(1024).splitlines()
        except (ConnectionRefusedError, TimeoutError, socket.error, socket.gaierror) as e:
            LOGGER.warning(f'Rotor error: {e}')
            self.rotor.close()
            return None
        if len(data) < 2:
            LOGGER.warning(f'bad response: {data}')
            return None
        if data[-1] != b'RPRT 0':
            LOGGER.warning(f'bad report: {data[-1]}')
            return None
        return ', '.join([i.decode() for i in data[1:-1]])

    def start_prepost(self, cmd: str):
        if len(self.prepost_script) > 0:
            script = [self.prepost_script, cmd]
            try:
                subprocess.Popen([self.prepost_script, cmd])
                LOGGER.info(f'Started prepost script: {script}')
            except (FileNotFoundError, TypeError) as e:
                LOGGER.error(f'Unable to launch prepost script {script}: {e}')

    def rotor_log(self, row: str):
        try:
            with open(self.rotlog_file, 'a', newline='') as lf:
                if lf.tell() == 0:
                    lf.write('sat_az,sat_alt,cmd_az,cmd_alt,rot_az,rot_alt\n')
                lf.write(row)
        except (FileNotFoundError, PermissionError, OSError) as e:
            LOGGER.error(f'csv logging failed: {e}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mode', type=int, default=0, help='Operating mode')
    parser.add_argument('-n', '--norad', type=int, default=0, help='NORAD ID to track')
    parser.add_argument('-r', '--samp_rate', type=float, default=48e3, help='Sample rate')
    parser.add_argument('-f', '--freq', type=float, default=400e6, help='Center frequency')
    args, oth = parser.parse_known_args()
    if len(oth) > 0:
        LOGGER.error(f'Warning: unknown arguments passed {oth}')
    Worker(args.mode, args.norad, args.samp_rate, args.freq).main()
