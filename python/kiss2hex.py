#!/usr/bin/env python3
import datetime
from os import path
from sys import argv
import struct


def main():
    if len(argv) != 2:
        print(f'Useage: {path.basename(argv[0])} <infile>\n'
              'Process a single KISS file and print the timestamped frames in HEX.\n')
        exit(0)
    if not path.exists(argv[1]):
        print(f'File not found: {argv[1]}')
        exit(1)
    with open(argv[1], 'rb') as f:
        for ts, frame in parse_kissfile(f):
            print(f'{ts.strftime("%Y-%m-%d %H:%M:%S")} | len: {len(frame)} | {frame.hex(bytes_per_sep=2).upper()}')
            # output format looks like GetKISS+


def parse_kissfile(infile):
    ts = datetime.datetime.now()  # MUST be overwritten by timestamps in file
    for row in infile.read().split(b'\xC0'):
        if len(row) == 9 and row[0] == 9:  # timestamp frame
            ts = datetime.datetime(1970, 1, 1) + datetime.timedelta(seconds=struct.unpack('>Q', row[1:])[0] / 1000)
        if len(row) > 0 and row[0] == 0:  # data frame
            yield ts, row[1:].replace(b'\xdb\xdc', b'\xc0').replace(b'\xdb\xdd', b'\xdb')


if __name__ == '__main__':
    main()
