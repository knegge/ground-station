#!/usr/bin/env python3
from sys import argv


def main():
    if len(argv) != 2:
        print(f'Useage: {argv[0]} <pre|post>')
        exit(0)

    if 'pre' in argv[1]:
        print('Pre script')

    elif 'post' in argv[1]:
        print('Post script')

    else:
        print('Unknown command')


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(f'Unhandled exception: {e}')
