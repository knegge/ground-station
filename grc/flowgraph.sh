#!/bin/bash
set -e

# ARGS
SAMP_RATE="${1:-48000}"
RX_FREQ="${2:-400e6}"
: "${FLOWGRAPH:=recorder.py}"

exec ${FLOWGRAPH} \
    ${RX_DEVICE:+ --rx-device "${RX_DEVICE}"} \
    ${DEV_ARGS:+ --dev-args "${DEV_ARGS}"} \
    ${STREAM_ARGS:+ --stream-args "${STREAM_ARGS}"} \
    ${RX_SAMP_RATE:+ --rx-samp-rate "${RX_SAMP_RATE}"} \
    ${TUNE_ARGS:+ --tune-args "${TUNE_ARGS}"} \
    ${OTHER_SETTINGS:+ --other-settings "${OTHER_SETTINGS}"} \
    ${RX_BW:+ --rx-bw "${RX_BW}"} \
    ${RX_ANTENNA:+ --rx-antenna "${RX_ANTENNA}"} \
    ${RX_FREQ:+ --rx-freq "${RX_FREQ}"} \
    ${LO_OFFSET:+ --lo-offset "${LO_OFFSET}"} \
    ${RX_PPM:+ --rx-ppm "${RX_PPM}"} \
    ${RX_GAIN:+ --rx-gain "${RX_GAIN}"} \
    ${DOPPLER_FILE_PATH:+ --doppler-file-path "${DOPPLER_FILE_PATH}"} \
    ${SAMP_RATE:+ --samp-rate "${SAMP_RATE}"} \
    ${UDP_SINK_HOST:+ --udp-sink-host "${UDP_SINK_HOST}"} \
    ${UDP_SINK_PORT:+ --udp-sink-port "${UDP_SINK_PORT}"} \
    ${IQ_FILE_PATH:+ --iq-file-path "${IQ_FILE_PATH}"} \
    ${WATERFALL_FILE_PATH:+ --waterfall-file-path "${WATERFALL_FILE_PATH}"} \
    ${WAV_GAIN:+ --wav-gain "${WAV_GAIN}"} \
    ${CLOCK_SOURCE:+ --clock-source "${CLOCK_SOURCE}"}
